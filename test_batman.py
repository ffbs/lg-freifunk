import re

import pytest

from labgrid.driver import ExecutionError

def test_batman_version(command):
    version = "2013.4.0"
    try:
        command.run_check('which batctl')
    except ExecutionError:
        pytest.skip("batctl missing")


    pattern = r"""batctl (?P<version>[0-9.]+)"""
    result = command.run_check("batctl -v")
    result = result[-1].strip()

    assert re.findall(pattern, result) == [version]

