import re

import pytest

from labgrid.driver import ExecutionError

def test_gluon_version(command):
    version = "v2016.2.5"
    try:
        command.run_check('which cat')
    except ExecutionError:
        pytest.skip("cat missing")

    stdout, stderr, returncode = command.run('cat /lib/gluon/gluon-version')

    assert stdout[-1] == version

@pytest.mark.parametrize("process", [ ("dnsmasq"),
                                      ("dropbear"),
                                      ("ntpd"),
                                      ("sse-multiplexd"),
                                      ("micrond"),
                                      ("odhcp6c"),
                                      ("netifd"),
                                      ("uradvd"),
                                      ("haveged"),
                                      ("batadv-vis"),
                                      ("respondd"),
                                      ("hostapd")])
def test_processes_available(command, process):
    try:
        command.run_check('which pidof')
    except ExecutionError:
        pytest.skip("pidof missing")
    stdout, stderr, returncode = command.run('pidof {}'.format(process))

    assert returncode == 0
    assert len(stdout) > 0
    assert len(stderr) == 0

@pytest.mark.parametrize("cmd", [("batctl"),("fastd"), ("alfred")])
def test_command_available(command, cmd):
    stdout, stderr, returncode = command.run('which {}'.format(cmd))

    assert returncode == 0
    assert len(stdout) > 0
    assert len(stderr) == 0


@pytest.mark.parametrize("dev", [('lo'), ('eth0'), ('eth1'), ('dummy0'), ('teql0'), ('br-client'), ('br-wan'), ('local-node@br-client'), ('ibss0'), ('client0'), ('primary0'), ('bat0')])
def test_devnames(command, dev):
    try:
        command.run_check('which ip')
    except ExecutionError:
        pytest.skip("ip missing")

    pattern = r"""\d+: (?P<devname>[A-Za-z0-9\-\@]+)"""

    result = command.run_check('ip l')
    result = "\n".join(result)

    actual_devs = re.findall(pattern, result)
    assert dev in actual_devs

@pytest.mark.parametrize("setting, value", [('wireless.client_radio0.disabled', '0'),
                                            ('wireless.client_radio0.ssid', 'Freifunk'),
                                            ('batman-adv.bat0', 'mesh'),
                                            ('batman-adv.bat0.gw_mode', 'client'),
                                            ('batman-adv.bat0.multicast_mode', '0'),
                                            ('wireless.client_radio0.mode', 'ap')])
def test_uci_setting(command, setting, value):
    try:
        command.run_check('which uci')
    except ExecutionError:
        pytest.skip("uci missing")

    result = command.run_check('uci get {}'.format(setting))
    result = "\n".join(result)

    assert result == value
